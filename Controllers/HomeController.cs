using Microsoft.AspNetCore.Mvc;
using PMSWebAppDemo.DBContext;
using PMSWebAppDemo.Models;
using System.Collections.ObjectModel;
using System.Diagnostics;

namespace PMSWebAppDemo.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private AppointmentDBContext appointmentContext = new AppointmentDBContext();

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        public IActionResult NewFeature()
        {
            return View();
        }

        public IActionResult CalenderView()
        {
            return View();
        }

        public IActionResult CalendarWithAppointment()
        {
            return View();
        }

        public IActionResult CalendarWithPlugins()
        {
            return View();
		}

		public IActionResult Calendar()
		{
			return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public List<AppointmentModel> GetAppointmentListByMonth(int month)
        {
            return appointmentContext.GetAppointmentListByMonth(month);
        }

        public List<Events> GetEvents(int month)
        {
            return appointmentContext.GetEvents();
        }

        public void SaveEvent(Events events)
        {
            appointmentContext.SaveEvents(events);
        }
    }
}
