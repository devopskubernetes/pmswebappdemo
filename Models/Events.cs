﻿using System.ComponentModel.DataAnnotations;

namespace PMSWebAppDemo.Models
{
    public class Events
    {
        [Key]
        public int EventId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime Start { get; set; }
        public DateTime? End { get; set; }
    }
}
