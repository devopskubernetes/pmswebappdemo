﻿namespace PMSWebAppDemo.Models
{
    public class AppointmentModel
    {
        public int AppointmentID { get; set; }
        public DateTime StartDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
        public string Remarks { get; set; }
    }
}
