﻿using Microsoft.EntityFrameworkCore;
using MySql.Data.MySqlClient;
using PMSWebAppDemo.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using static Org.BouncyCastle.Math.EC.ECCurve;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace PMSWebAppDemo.DBContext
{
    public class AppointmentDBContext : DbContext
    {
        private readonly IConfiguration config = Host.CreateApplicationBuilder().Configuration;

        public DbSet<Events> Mst_CalendarWithPlugins { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseMySql(config.GetConnectionString("DefaultConnection"), ServerVersion.AutoDetect(config.GetConnectionString("DefaultConnection")));
        }

        private MySqlConnection Connection
        {
            get
            {
                return new MySqlConnection("Server=localhost;Database=PMSdb;User=root;Password=password;");
            }
        }

        public List<AppointmentModel> GetAppointmentListByMonth(int month)
        {
            List<AppointmentModel> sList = new List<AppointmentModel>();

            string startDate = "2024-" + month + "-1";
            int lastDay = DateTime.DaysInMonth(2024,month);
            string endDate = "2024-" + month + "-" + lastDay + " 23:59:59";
            string dateRange = "startDateTime between '" + startDate + "' and '"+endDate+"'";

            using (MySqlConnection conn = this.Connection)
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("Select * from mst_appointment where "+ dateRange, conn);

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        sList.Add(new AppointmentModel()
                        {
                            AppointmentID = reader.GetInt32(0),
                            StartDateTime = reader.GetDateTime(1),
                            EndDateTime = reader.GetDateTime(2),
                            Remarks = reader.GetString(3)
                        });
                    }
                }
            }

            return sList;
        }

        public List<Events> GetEvents()
        {
            List<Events> sList = Mst_CalendarWithPlugins.ToList();            

            return sList;
        }

        public void SaveEvents(Events events)
        {
            var e = Mst_CalendarWithPlugins.FirstOrDefault(x => x.EventId == events.EventId);

            if (e != null)
            {
                e.Title = events.Title;
                e.Description = events.Description;
                e.Start = events.Start;
                e.End = events.End;
            }

            SaveChanges();
        }
    }
}
